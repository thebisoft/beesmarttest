<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->group(['prefix' => '/product'], function (Router $router) {
    $router->get('products', [
        'as' => 'api.product.product.index',
        'uses' => 'ProductController@index',
    ]);
});
