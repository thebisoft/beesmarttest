<?php

namespace Modules\Product\Repositories\Eloquent;

use Modules\Product\Events\ProductIsCreating;
use Modules\Product\Events\ProductIsUpdating;
use Modules\Product\Events\ProductWasCreated;
use Modules\Product\Events\ProductWasDeleted;
use Modules\Product\Events\ProductWasUpdated;
use Modules\Product\Repositories\ProductRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentProductRepository extends EloquentBaseRepository implements ProductRepository
{
    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        event($event = new ProductIsCreating($data));
        $product = $this->model->create($event->getAttributes());

        event(new ProductWasCreated($product, $data));

        return $product;
    }

    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data)
    {
        event($event = new ProductIsUpdating($model, $data));
        $model->update($event->getAttributes());

        event(new ProductWasUpdated($model, $data));

        return $model;
    }

    /**
     * @param $product
     * @return bool
     */
    public function destroy($product)
    {
        event(new ProductWasDeleted($product));

        return $product->delete();
    }
}
