<div class="box-body">
    <p>
        {!! Form::normalInput('name', 'Name', $errors) !!}
        {!! Form::normalInput('price', 'Price', $errors) !!}
        {!! Form::normalTextarea('description', 'Description', $errors) !!}
        @mediaSingle('image')
    </p>
</div>
