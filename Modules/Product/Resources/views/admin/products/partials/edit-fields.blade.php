<div class="box-body">
    <p>
        {!! Form::normalInput('name', 'Name', $errors, $product) !!}
        {!! Form::normalInput('price', 'Price', $errors, $product) !!}
        {!! Form::normalTextarea('description', 'Description', $errors, $product) !!}
        @mediaSingle('image', $product)
    </p>
</div>
