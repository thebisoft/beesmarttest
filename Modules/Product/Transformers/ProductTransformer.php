<?php

namespace Modules\Product\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Media\Image\Imagy;

class ProductTransformer extends Resource
{
    public function toArray($request)
    {
        $imagy = app('imagy');

        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'price' => $this->resource->price,
            'description' => $this->resource->description,
            'image' => $imagy->getThumbnail($this->resource->image, 'mediumThumb'),
            'created_at' => $this->resource->created_at->format('d-m-Y'),
        ];
    }
}
