<?php

namespace Modules\Product\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;

class Product extends Model
{
    use MediaRelation;

    protected $table = 'product__products';
    protected $fillable = ['name', 'price', 'description'];

    /**
     * @return string
     */
    public function getImageAttribute()
    {
        $thumbnail = $this->files()->where('zone', 'image')->first();

        if ($thumbnail === null) {
            return '';
        }

        return $thumbnail->path;
    }

    /**
     * @param $value
     * @return float
     */
    public function getPriceAttribute($value)
    {
        return floatval($value);
    }
}
